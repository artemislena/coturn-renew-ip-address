# Refresh public IP address for Coturn

A small script that renews the IP address in the Coturn config for anyone who is using it behind a NAT with a dynamic IP address. Especially helpful for Synapse users and such. Reason described in https://gist.github.com/maxidorius/2b0acc2e707ae9a2d6d0267026a1024f

Just download the script, make it executable, change the domain from turn.example.org to whatever you are using, and run it via a Cronjob. If you are not using Runit, you will also have to change `sv restart coturnserver` to whatever your init system/service manager uses. Also make sure `/etc/coturn/` exists on your system and is writable.
